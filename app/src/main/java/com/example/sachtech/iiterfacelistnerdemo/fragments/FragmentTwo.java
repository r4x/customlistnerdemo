package com.example.sachtech.iiterfacelistnerdemo.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.sachtech.iiterfacelistnerdemo.ClickListnerImpl;
import com.example.sachtech.iiterfacelistnerdemo.R;


public class FragmentTwo extends Fragment {

    Button button;
    EditText editText;
    ClickListnerImpl listner;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frag_two, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        editText = view.findViewById(R.id.e2);
        listner = ClickListnerImpl.getInstance();
        listner.setTag(getActivity().getClass().getSimpleName());
        editText.addTextChangedListener(new TextWatcher() {
         @Override
         public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

         }

         @Override
         public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

         }

         @Override
         public void afterTextChanged(Editable editable) {
             if (!editable.toString().equals("")){
                 listner.secound(Integer.parseInt(editable.toString()));}
             else{
                 listner.secound(0);
             }
         }
     });
    }
}
