package com.example.sachtech.iiterfacelistnerdemo;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.sachtech.iiterfacelistnerdemo.fragments.FragmentOne;
import com.example.sachtech.iiterfacelistnerdemo.fragments.FragmentThree;
import com.example.sachtech.iiterfacelistnerdemo.fragments.FragmentTwo;

public class MainActivity extends AppCompatActivity {
    FragmentManager fm;
    public ClickListnerImpl clickListner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }


    private void init() {
        fm = getFragmentManager();
        FragmentTransaction fff = fm.beginTransaction();
        fff.replace(R.id.frag1, new FragmentOne());
        fff.replace(R.id.frag2, new FragmentTwo());
        fff.replace(R.id.frag3, new FragmentThree());
        fff.commit();
    }

    public void nextActivity(View view) {
        startActivity(new Intent(MainActivity.this, SubtractionActivity.class));
    }
}
