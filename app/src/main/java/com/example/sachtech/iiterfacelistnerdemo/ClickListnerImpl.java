package com.example.sachtech.iiterfacelistnerdemo;

public class ClickListnerImpl implements CLickListner {
    int a,b;
    TextChangeListner textChangeListner;
    private static ClickListnerImpl clickListner;
    // TODO: 15-Nov-17  remove this shit find something else
    String ClassTag = "MainActivity";

    private ClickListnerImpl() {

    }

    public static ClickListnerImpl getInstance() {
        if (clickListner == null) {
            clickListner = new ClickListnerImpl();
        }
        return clickListner;
    }

    public void setTextChangeListner(TextChangeListner listner) {
        textChangeListner = listner;
    }

    @Override
    public void first(int a) {
        this.a = a;
        textChangeListner.onTextChangeCall();
    }

    @Override
    public void secound(int b) {
        this.b = b;
        textChangeListner.onTextChangeCall();
    }

    @Override
    public int getComputeResult() {
        if (this.ClassTag.equals("MainActivity")) {
            return this.a + this.b;
        }
        return this.a - this.b;
    }

    @Override
    public void setTag(String tag) {
        this.ClassTag = tag;
    }
}
