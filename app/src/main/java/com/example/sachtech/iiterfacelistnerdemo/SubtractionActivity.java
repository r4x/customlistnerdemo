package com.example.sachtech.iiterfacelistnerdemo;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.sachtech.iiterfacelistnerdemo.fragments.FragmentOne;
import com.example.sachtech.iiterfacelistnerdemo.fragments.FragmentThree;
import com.example.sachtech.iiterfacelistnerdemo.fragments.FragmentTwo;

public class SubtractionActivity extends AppCompatActivity {
    FragmentManager fm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subtraction);
        init();
    }
    private void init() {
        fm = getFragmentManager();
        FragmentTransaction fff = fm.beginTransaction();
        fff.replace(R.id.fragOne,new FragmentOne());
        fff.replace(R.id.fragTwo,new FragmentTwo());
        fff.replace(R.id.fragThree,new FragmentThree());
        fff.commit();
    }

}
