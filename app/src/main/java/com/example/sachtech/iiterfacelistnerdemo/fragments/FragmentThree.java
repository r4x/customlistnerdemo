package com.example.sachtech.iiterfacelistnerdemo.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.sachtech.iiterfacelistnerdemo.ClickListnerImpl;
import com.example.sachtech.iiterfacelistnerdemo.R;
import com.example.sachtech.iiterfacelistnerdemo.TextChangeListner;


public class FragmentThree extends Fragment implements TextChangeListner {
    Button button;
    TextView editText;
    ClickListnerImpl listner;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frag_three, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        editText = view.findViewById(R.id.e3);
        listner = ClickListnerImpl.getInstance();
        listner.setTextChangeListner(FragmentThree.this);
    }

    @Override
    public void onTextChangeCall() {
        editText.setText(listner.getComputeResult() + "");
    }
}
