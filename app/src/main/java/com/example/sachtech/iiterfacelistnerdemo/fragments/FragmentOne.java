package com.example.sachtech.iiterfacelistnerdemo.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.sachtech.iiterfacelistnerdemo.CLickListner;
import com.example.sachtech.iiterfacelistnerdemo.ClickListnerImpl;
import com.example.sachtech.iiterfacelistnerdemo.R;

// TODO: 15-Nov-17  create only one fragment instead of three and use there instance
public class FragmentOne extends Fragment {
    EditText editText;
    CLickListner listner;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frag_one, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //TODO remove from constants
        listner = ClickListnerImpl.getInstance();
        editText = view.findViewById(R.id.e1);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                // TODO: 15-Nov-17  make more efficienct
                if (!editable.toString().equals("")) {
                    listner.first(Integer.parseInt(editable.toString()));
                } else {
                    listner.first(0);
                }
            }
        });
    }
}
